CREATE DATABASE  IF NOT EXISTS `festival` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `festival`;
-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: festival
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artesanias`
--

DROP TABLE IF EXISTS `artesanias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `artesanias` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) NOT NULL,
  `Descripcion` text NOT NULL,
  `Estado` enum('Activo','Inactivo','Eliminado') NOT NULL DEFAULT 'Activo',
  `FechaAdicion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Foto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artesanias`
--

LOCK TABLES `artesanias` WRITE;
/*!40000 ALTER TABLE `artesanias` DISABLE KEYS */;
/*!40000 ALTER TABLE `artesanias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artesanias_has_puntuacion`
--

DROP TABLE IF EXISTS `artesanias_has_puntuacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `artesanias_has_puntuacion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idArtesania` int NOT NULL,
  `idPuntuacion` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbComentarios_has_tbArtesanias_tbArtesanias1_idx` (`idArtesania`),
  KEY `fk_tbComentarios_has_tbArtesanias_tbComentarios1_idx` (`idPuntuacion`),
  CONSTRAINT `fk_tbComentarios_has_tbArtesanias_tbArtesanias1` FOREIGN KEY (`idArtesania`) REFERENCES `artesanias` (`id`),
  CONSTRAINT `fk_tbComentarios_has_tbArtesanias_tbComentarios1` FOREIGN KEY (`idPuntuacion`) REFERENCES `puntuacion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artesanias_has_puntuacion`
--

LOCK TABLES `artesanias_has_puntuacion` WRITE;
/*!40000 ALTER TABLE `artesanias_has_puntuacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `artesanias_has_puntuacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boletos`
--

DROP TABLE IF EXISTS `boletos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `boletos` (
  `id` int NOT NULL,
  `Stock` int NOT NULL,
  `Precio` int NOT NULL,
  `Estado` enum('Activo','Inactivo','Eliminado') NOT NULL,
  `FechaAdicion` datetime NOT NULL,
  `Token` varchar(100) NOT NULL,
  `tbEventos_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbBoletos_tbEventos1_idx` (`tbEventos_id`),
  CONSTRAINT `fk_tbBoletos_tbEventos1` FOREIGN KEY (`tbEventos_id`) REFERENCES `eventos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boletos`
--

LOCK TABLES `boletos` WRITE;
/*!40000 ALTER TABLE `boletos` DISABLE KEYS */;
/*!40000 ALTER TABLE `boletos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eventos` (
  `id` int NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Descripcion` text NOT NULL,
  `Cupo` int NOT NULL,
  `Fecha` date NOT NULL,
  `Hora` time NOT NULL,
  `FechaAdicion` datetime NOT NULL,
  `Estado` enum('Activo','Inactivo','Eliminado') NOT NULL,
  `Foto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos`
--

LOCK TABLES `eventos` WRITE;
/*!40000 ALTER TABLE `eventos` DISABLE KEYS */;
/*!40000 ALTER TABLE `eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventos_has_puntuacion`
--

DROP TABLE IF EXISTS `eventos_has_puntuacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eventos_has_puntuacion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idEvento` int NOT NULL,
  `idComentario` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbEventos_has_tbComentarios_tbComentarios1_idx` (`idComentario`),
  KEY `fk_tbEventos_has_tbComentarios_tbEventos1_idx` (`idEvento`),
  CONSTRAINT `fk_tbEventos_has_tbComentarios_tbComentarios1` FOREIGN KEY (`idComentario`) REFERENCES `puntuacion` (`id`),
  CONSTRAINT `fk_tbEventos_has_tbComentarios_tbEventos1` FOREIGN KEY (`idEvento`) REFERENCES `eventos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos_has_puntuacion`
--

LOCK TABLES `eventos_has_puntuacion` WRITE;
/*!40000 ALTER TABLE `eventos_has_puntuacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `eventos_has_puntuacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `festivales`
--

DROP TABLE IF EXISTS `festivales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `festivales` (
  `id` int NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `descripcion` text NOT NULL,
  `fotos` text NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `festivales`
--

LOCK TABLES `festivales` WRITE;
/*!40000 ALTER TABLE `festivales` DISABLE KEYS */;
/*!40000 ALTER TABLE `festivales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lugares`
--

DROP TABLE IF EXISTS `lugares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lugares` (
  `id` int NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Descripcion` text NOT NULL,
  `Foto` text NOT NULL,
  `Estado` enum('Activo','Inactivo','Eliminado') NOT NULL,
  `Latitud` decimal(10,8) NOT NULL,
  `Longitud` decimal(10,8) NOT NULL,
  `Views` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lugares`
--

LOCK TABLES `lugares` WRITE;
/*!40000 ALTER TABLE `lugares` DISABLE KEYS */;
/*!40000 ALTER TABLE `lugares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lugares_has_puntuacion`
--

DROP TABLE IF EXISTS `lugares_has_puntuacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lugares_has_puntuacion` (
  `id` int NOT NULL,
  `tbLugares_id` int NOT NULL,
  `tbComentarios_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbLugares_has_tbComentarios_tbComentarios1_idx` (`tbComentarios_id`),
  KEY `fk_tbLugares_has_tbComentarios_tbLugares1_idx` (`tbLugares_id`),
  CONSTRAINT `fk_tbLugares_has_tbComentarios_tbComentarios1` FOREIGN KEY (`tbComentarios_id`) REFERENCES `puntuacion` (`id`),
  CONSTRAINT `fk_tbLugares_has_tbComentarios_tbLugares1` FOREIGN KEY (`tbLugares_id`) REFERENCES `lugares` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lugares_has_puntuacion`
--

LOCK TABLES `lugares_has_puntuacion` WRITE;
/*!40000 ALTER TABLE `lugares_has_puntuacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `lugares_has_puntuacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `puntuacion`
--

DROP TABLE IF EXISTS `puntuacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `puntuacion` (
  `id` int NOT NULL,
  `idUsuario` int NOT NULL,
  `Puntos` int NOT NULL,
  `Comentario` text NOT NULL,
  `FechaAdicion` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbComentarios_tbUsuarios_idx` (`idUsuario`),
  CONSTRAINT `fk_tbComentarios_tbUsuarios` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `puntuacion`
--

LOCK TABLES `puntuacion` WRITE;
/*!40000 ALTER TABLE `puntuacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `puntuacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(20) NOT NULL,
  `Apellidos` varchar(35) NOT NULL,
  `Genero` enum('Hombre','Mujer','No especifico') NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `Correo` varchar(30) NOT NULL,
  `Telefono` char(10) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `TipoUsuario` enum('Administrador','Visitante','Staff') NOT NULL,
  `Estado` enum('Activo','Inactivo','Eliminado') NOT NULL DEFAULT 'Inactivo',
  `Foto` text,
  `FechaAdicion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Token` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Correo_UNIQUE` (`Correo`),
  UNIQUE KEY `Telefono_UNIQUE` (`Telefono`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Gustavo','Leon Santos','Hombre','1999-07-23','leonsantosgustavo@gmail.com','7761540362','$2y$10$0a2LCMOEyIuCmJV2i5tCt.uHvcAD1X/EC8dO98hoXM4WP2u/j6.xi','Administrador','Inactivo','','2021-07-27 23:41:37','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2Mjc2ODU4NzksImV4cCI6MTYyNzY4OTQ3OSwiZGF0YSI6eyJ1c3VhcmlvIjoibGVvbnNhbnRvc2d1c3Rhdm9AZ21haWwuY29tIiwicGFzc3dvcmQiOiIxMjM0IiwiaWQiOiIxIn19.w1jq5am8-nX_RDfrArNsQggYQQGScUmk1n69suQqafg');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_has_boletos`
--

DROP TABLE IF EXISTS `usuarios_has_boletos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_has_boletos` (
  `id` int NOT NULL,
  `idUsuario` int NOT NULL,
  `idBoleto` int NOT NULL,
  `FechaCompra` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbUsuarios_has_tbBoletos_tbBoletos1_idx` (`idBoleto`),
  KEY `fk_tbUsuarios_has_tbBoletos_tbUsuarios1_idx` (`idUsuario`),
  CONSTRAINT `fk_tbUsuarios_has_tbBoletos_tbBoletos1` FOREIGN KEY (`idBoleto`) REFERENCES `boletos` (`id`),
  CONSTRAINT `fk_tbUsuarios_has_tbBoletos_tbUsuarios1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_has_boletos`
--

LOCK TABLES `usuarios_has_boletos` WRITE;
/*!40000 ALTER TABLE `usuarios_has_boletos` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_has_boletos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-12 20:35:27
